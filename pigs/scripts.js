function getPigIndex(){
    var probabilities = [0.349, 0.302, 0.224, 0.088, 0.03, 0.0061];
    var states = [0, 1, 2, 3, 4, 5];

    var rnd = Math.random();
    var total = 0;
    var hit = -1;

    for(var i = 0; i < probabilities.length; i++){
      if(rnd > total && rnd < total + probabilities[i]){
           hit = states[i]
      }
      total += probabilities[i];
    }
    return [hit,rnd];
}

var pigArray = 
[
    ["nodot.png", "dot.png", "razorback.png", "trotter.png", "snouter.png", "leaningjowler.png"],
    ["Side (No Dot)", "Side (Dot)", "Razorback", "Trotter", "Snouter", "LEANING JOWLER!!"]
];

function setRollPigImg(){
    var pigIndex1 = getPigIndex();
    var pigValue1 = document.getElementById("pigValue1");
    var pigImg1 = document.getElementById("pigImg1");
    var pigCaption1 = document.getElementById("pigCaption1");
    
    pigValue1.innerHTML = pigIndex1[1];
    pigImg1.src = 'images/'+ pigArray[0][pigIndex1[0]];
    pigCaption1.innerHTML = pigArray[1][pigIndex1[0]];

    var pigIndex2 = getPigIndex();
    var pigValue2 = document.getElementById("pigValue2");
    var pigImg2 = document.getElementById("pigImg2");
    var pigCaption2 = document.getElementById("pigCaption2");

    pigValue2.innerHTML = pigIndex2[1];
    pigImg2.src = 'images/'+ pigArray[0][pigIndex2[0]];
    pigCaption2.innerHTML = pigArray[1][pigIndex2[0]];
}